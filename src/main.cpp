#include <iostream>
#include <boost/thread/thread.hpp>
#include <pcl/common/common_headers.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/console/parse.h>
#include "bpc_loader.h"

int main(int argc, char** argv)
{
	auto data_file = std::string{ "" };
	if (pcl::console::parse_argument(argc, argv, "-d", data_file) < 0)
	{
		std::cout << "Must provide input file." << std::endl;
		return 0;
	}

	auto loader = BPCLoader{ data_file };
	auto cloud = pcl::PointCloud<pcl::PointXYZRGB>::Ptr{ new pcl::PointCloud<pcl::PointXYZRGB> };

	if (!loader.readSingleFrame(cloud))
	{
		std::cout << "Failed to read input file." << std::endl;
		return 0;
	}

	auto viewer = boost::shared_ptr<pcl::visualization::PCLVisualizer>{ new pcl::visualization::PCLVisualizer("3D Viewer") };
	viewer->setBackgroundColor(0, 0, 0);
	auto rgb = pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB>{ cloud };
	viewer->addPointCloud<pcl::PointXYZRGB>(cloud, rgb, "sample cloud");
	viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 3, "sample cloud");
	viewer->addCoordinateSystem(1.0);
	viewer->setCameraPosition(0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, -1.0);

	while (!viewer->wasStopped())
	{
		viewer->spinOnce(100);
		boost::this_thread::sleep(boost::posix_time::microseconds(100000));
	}
}