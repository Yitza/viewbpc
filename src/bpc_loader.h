#pragma once
#include <string>
#include <fstream>
#include <vector>
#include <pcl/common/common_headers.h>
#include "bpc_packet.h"

#define PACKET_SIZE 1448
#define NUMBER_OF_POINT 160

class BPCLoader {
public:
	BPCLoader() = delete;
	BPCLoader(std::string filename);

	bool readSingleFrame(pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud);

private:

	bool readPacket(std::array<char, PACKET_SIZE>& packet);
	void parsePacket(char const * buf, BPC_Header& head, std::array<BPC_Point, NUMBER_OF_POINT>& bpc_points);

private:
	std::ifstream _file;
	int _init_theta;
	bool _init_theta_set;
};

pcl::PointXYZRGB convertBPCPoint2PCL(const BPC_Point& point);