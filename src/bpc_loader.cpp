#include "bpc_loader.h"
#include <stddef.h>
#include <array>
#include <algorithm>

#define FOV 900000
#define INVALID_RANGE 0x7FFFF

BPCLoader::BPCLoader(std::string filename)
	: _init_theta{ 0 }
	, _init_theta_set{ false }
{
	_file = std::ifstream{ filename, std::ios::binary };
}

// reads packet data until a full FOV of data has been read.
// return whether reading was successfull.
bool BPCLoader::readSingleFrame(pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud)
{
	auto buf = std::array<char, PACKET_SIZE>{ 0 };
	auto still_reading = bool{ true };
	while (still_reading)
	{
		if (!readPacket(buf))
		{
			return false;
		}
		auto packet_points = std::array<BPC_Point, NUMBER_OF_POINT>{};
		auto packet_header = BPC_Header{};

		parsePacket(buf.data(), packet_header, packet_points);

		if (!_init_theta_set)
		{
			// find min theta value in initial packet and initialize as theta0
			auto min_theta_it = std::min_element(packet_points.begin(), packet_points.end(), [](auto a, auto b) { return a.theta < b.theta; });
			_init_theta = min_theta_it->theta;
			_init_theta_set = true;
		}

		for (auto i : packet_points)
		{
			if ((i.distance & INVALID_RANGE) != INVALID_RANGE && i.sensorhead == 2)
			{
				// normalize theta to account for angle wrapping
				auto normailze_theta = i.theta + (i.theta < _init_theta ? FOV : 0);
				// add points while accumulated theta is less than FOV
				if (normailze_theta - _init_theta < FOV - 1000)
				{
					auto pcl_point = convertBPCPoint2PCL(i);
					cloud->push_back(pcl_point);
				}
				else
				{
					still_reading = false;
					break;
				}
			}
		}
	}
	return true;
}

// read a single packet 1448 bytes from file
bool BPCLoader::readPacket(std::array<char, PACKET_SIZE>& packet)
{
	_file.read(packet.data(), PACKET_SIZE);

	if (_file)
	{
		return true;
	}
	return false;
}

// parse packet with bpc_packet functions
void BPCLoader::parsePacket(char const * buf, BPC_Header& head, std::array<BPC_Point, NUMBER_OF_POINT>& bpc_points)
{
	bpc_fill_header(reinterpret_cast<uint8_t const *>(buf), PACKET_SIZE, &head);
	bpc_fill_points(reinterpret_cast<uint8_t const *>(buf), PACKET_SIZE, bpc_points.data(), NUMBER_OF_POINT);
	return;
}

// conversion function for BPC_Point to PointXYZRGB
pcl::PointXYZRGB convertBPCPoint2PCL(const BPC_Point & point)
{
	float theta_rad = point.theta * M_PI / (180.0 * 10000);
	float phi_rad = point.phi * M_PI / (180.0 * 10000);

	auto pcl_point = pcl::PointXYZRGB{};
	pcl_point.x = point.distance * cos(phi_rad) * cos(theta_rad) / 1000.0;
	pcl_point.y = point.distance * cos(phi_rad) * sin(theta_rad) / 1000.0;
	pcl_point.z = point.distance * sin(phi_rad) / 1000.0;
	pcl_point.r = point.intensity;
	pcl_point.g = point.intensity;
	pcl_point.b = point.intensity;

	return pcl_point;
}
