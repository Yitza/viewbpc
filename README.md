### Installation ###

This project has dependency on the following:

* CMake
* PCL 1.9.1

The project used the visual studio 2017 all in one build of PCL 1.9.1, and was not tested in any other environment.

### Running the app ###

After building the app, simply run visualiser.exe with the following arguement `-d <input file>`
eg. `visualiser.exe -d C:\Repos\viewbpc\data\record-2018-08-15_23-20-14-134.bin`
